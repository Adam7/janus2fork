<?php
/**
 * @file
 * Theme implementation to display a single Drupal page that is custom for The Gaming Blog.
 */
//dsm($page);
?>

<div id="portalNav">
  <div id="center">

    <div class="PN_leftCol">
      <a href="http://www.unlv.edu" title="UNLV"><img src="http://gaming.unlv.edu/Include/header/images/logo.png" border="0" width="58" height="21" alt="UNLV"></a>
    </div>
    <!-- end .PN_leftCol -->

    <div class="PN_rightCol">

      <ul id="qm0" class="qmmc">

        <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('audience_dropdown',true);" onmouseout="toggleMenu('audience_dropdown',false);" title="Information For">Information&nbsp;For&nbsp;»</a>
          <ul id="audience_dropdown" onmouseover="toggleMenu('audience_dropdown',true);" onmouseout="toggleMenu('audience_dropdown',false);">
            <li><a href="http://www.unlv.edu/futurestudents" title="Future Students">Future Students</a></li>
            <li><a href="http://www.unlv.edu/currentstudents" title="Current Students">Current Students</a></li>
            <li><a href="http://www.unlv.edu/alumni" title="Alumni">Alumni</a></li>
            <li><a href="http://www.unlv.edu/facultystaff" title="Faculty/Staff">Faculty/Staff</a></li>
            <li><a href="http://www.unlv.edu/donors" title="Donors">Donors</a></li>
            <li><a href="http://www.unlv.edu/community" title="Community">Community</a></li>
          </ul>
        </li>

        <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('topic_dropdown',true);" onmouseout="toggleMenu('topic_dropdown',false);" title="Topics">Topics&nbsp;»</a>
          <ul id="topic_dropdown" onmouseover="toggleMenu('topic_dropdown',true);" onmouseout="toggleMenu('topic_dropdown',false);">
            <li><a href="http://www.unlv.edu/about" title="About UNLV">About UNLV</a></li>
            <li><a href="http://www.unlv.edu/about/academics" title="Academics">Academics</a></li>
            <li><a href="http://www.unlv.edu/about/administration" title="Administration">Administration</a></li>
            <li><a href="http://web.unlv.edu/admissions" title="Admissions">Admissions</a></li>
            <li><a href="http://www.unlv.edu/about/athletics" title="Athletics">Athletics</a></li>
            <li><a href="http://foundation.unlv.edu/give.html" title="Give to UNLV">Give to UNLV</a></li>
            <li><a href="http://library.unlv.edu/" title="Libraries">Libraries</a></li>
            <li><a href="http://research.unlv.edu/" title="Research">Research</a></li>
          </ul>
        </li>

        <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('quicklinks_dropdown',true);" onmouseout="toggleMenu('quicklinks_dropdown',false);" title="Quick Links">Quick&nbsp;Links&nbsp;»</a>
          <ul id="quicklinks_dropdown" onmouseover="toggleMenu('quicklinks_dropdown',true);" onmouseout="toggleMenu('quicklinks_dropdown',false);">
            <li><a href="http://unlv.bncollege.com/" title="Bookstore">Bookstore</a></li>
            <li><a href="http://library.unlv.edu/" title="Libraries">Libraries</a></li>
            <li><a href="http://lotusnotes.oit.unlv.edu/" title="Lotus Notes">Lotus Notes</a></li>
            <li><a href="http://www.unlv.edu/maps" title="Maps &amp; Parking">Maps &amp; Parking</a></li>
            <li><a href="https://my.unlv.nevada.edu/" title="MyUNLV">MyUNLV</a></li>
            <li><a href="http://rebelcard.unlv.edu/" title="RebelCard">RebelCard</a></li>
            <li><a href="http://rebelmail.unlv.edu/" title="Rebelmail">Rebelmail</a></li>
            <li><a href="https://webcampus.unlv.edu/" title="WebCampus">WebCampus</a></li>
          </ul>
        </li>

        <li>
          <div id="search"><div id="___gcse_0"><div class="gsc-control-searchbox-only gsc-control-searchbox-only-en" dir="ltr"><form class="gsc-search-box" accept-charset="utf-8"><table cellspacing="0" cellpadding="0" class="gsc-search-box"><tbody><tr><td class="gsc-input"><input autocomplete="off" type="text" size="10" class=" gsc-input" name="search" title="search" id="gsc-i-id1" dir="ltr" spellcheck="false" style="outline: none; background-image: url(http://www.google.com/cse/intl/en/images/google_custom_search_watermark.gif); background-color: rgb(255, 255, 255); background-position: 0% 50%; background-repeat: no-repeat no-repeat; "></td><td class="gsc-search-button"><input type="button" value="Search" class="gsc-search-button" title="search"></td><td class="gsc-clear-button"><div class="gsc-clear-button" title="clear results">&nbsp;</div></td></tr></tbody></table><table cellspacing="0" cellpadding="0" class="gsc-branding"><tbody><tr><td class="gsc-branding-user-defined"></td><td class="gsc-branding-text"><div class="gsc-branding-text">powered by</div></td><td class="gsc-branding-img"><img src="http://www.google.com/uds/css/small-logo.png" class="gsc-branding-img"></td></tr></tbody></table></form></div></div></div>
        </li>

        <li class="qmclear">&nbsp;</li>

      </ul>

    </div>
    <!-- end PN_rightCol -->

    <div class="PN_clear"></div>

  </div>
  <!--end #center-->

</div>
<!-- end portalNav -->


<script type="text/javascript">
  <!--
  function MM_jumpMenuGo(objId,targ,restore){ //v9.0
    var selObj = null;  with (document) {
      if (getElementById) selObj = getElementById(objId);
      if (selObj) eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
      if (restore) selObj.selectedIndex=0; }
  }
  //-->
</script><style type="text/css">
  <!--
  body {
    background-image: url();
  }
  -->
</style>


<table width="1000" height="113" border="0" align="center" cellpadding="3" cellspacing="0" style="clear:both;">
  <tbody>
  <tr>
    <td height="53" colspan="4" bgcolor="#F6F6F6"><div align="center"><a href="http://gaming.unlv.edu"><img src="http://gaming.unlv.edu/cgr_head.jpg" alt="Center for Gaming Research" width="996" height="80" border="0" usemap="#unlv"></a>
    </div>
      <div align="center"></div></td>
  </tr>
  <tr>
    <td width="483" height="25" bgcolor="#F6F6F6">
      <div id="foot">
        <div align="left">
          <a href="http://gaming.unlv.edu/reports.html">Reports</a> | <a href="http://gaming.unlv.edu/papers.html">Papers</a>  | <a href="http://gaming.unlv.edu/podcast.html">Podcast</a> | <a href="http://gaming.unlv.edu/v_museum/index.html">Exhibits</a> | <a href="http://gaming.unlv.edu/about/fellowship.html">Fellowships</a> | <a href="http://gaming.unlv.edu/about/events.html">Events</a> | <a href="http://gaming.unlv.edu/update.html">Update</a> | <a href="https://www.library.unlv.edu/center_for_gaming_research/">Blog</a>
        </div>
      </div>
    </td>
    <td width="24" valign="middle" bgcolor="#F6F6F6">
    </td>
    <td width="411" height="25" bgcolor="#F6F6F6"><div align="right">
      <div id="foot">
        <div align="right"><a href="http://gaming.unlv.edu/jurisdictions.html">Jurisdictions</a> | <a href="http://gaming.unlv.edu/companies.html">Companies</a> | <a href="casinomath.html">Casino Math</a> | <a href="respongamb.html">Responsible Gambling</a></div>
      </div></div>
    </td>
    <td width="60" bgcolor="#F6F6F6">
      <div align="right">
        <a href="http://www.facebook.com/UNLVGamingResearch"><img src="http://gaming.unlv.edu/FB2.jpg" alt="UNLVGamingReseach on Facebook" width="20" height="20" border="0"></a> <a href="http://twitter.com/#!/unlvgaming"><img src="http://gaming.unlv.edu/TW.jpg" alt="UNLVGaming on Twitter" width="20" height="20" border="0"></a>
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#F6F6F6">
      <hr>
    </td>
  </tr>
  </tbody>
</table>

<map name="unlv" id="unlv"><area shape="circle" coords="-20,-16,24" href="http://www.unlv.edu" alt="UNLV Home">
  <area shape="rect" coords="5,-4,165,75" href="http://www.unlv.edu" alt="UNLV home">
  <area shape="rect" coords="166,2,766,71" href="http://gaming.unlv.edu" alt="Center for Gaming Research">
  <area shape="rect" coords="773,10,995,72" href="http://www.library.unlv.edu/" alt="University Librarites">
</map>
<!--<table width="1000" height="31" border="0" align="center" cellpadding="1" cellspacing="2" bgcolor="F6F6F6">-->
<!--<hr>-->
<?php print render($page['content']); ?>

<span class="style40">
  <table width="1000" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#F6F6F6">
    <tbody>
    <tr>
      <td colspan="2"><hr></td>
    </tr>
    <tr>
      <td width="710"><div id="foot">
        <div align="left">
          <p><a href="http://gaming.unlv.edu/about/index.html">About the Center</a> | <a href="http://gaming.unlv.edu/about/contact.html">Contact and Visiting</a> | <a href="http://www.library.unlv.edu/speccol/index.html">UNLV Special Collections</a> | <a href="http://www.library.unlv.edu/">University Libraries</a> | <a href="http://www.unlv.edu">UNLV home</a></p>
          <p>Follow <a href="http://www.twitter.com/unlvgaming">UNLVgaming</a> on <a href="http://www.twitter.com/unlvgaming"><img src="http://twitter-badges.s3.amazonaws.com/t_mini-b.png" alt="Follow unlvgaming on Twitter" border="0"></a> Twitter and <a href="http://www.facebook.com/UNLVGamingResearch">UNLVGamingResearch</a> on <a href="http://www.facebook.com/UNLVGamingResearch"><img src="http://gaming.unlv.edu/fb.jpg" width="18" height="18" border="0"></a> Facebook</p>
        </div>
      </div></td>
      <td width="290" valign="bottom"><div align="right"><font size="-1" face="Arial, Helvetica, sans-serif"><font color="#000000"><font color="#FFFFFF"><font color="#000000"><font color="#666666" size="2">© 2012 University of Nevada, Las Vegas</font></font> &gt;</font></font></font></div></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center"><font color="#666666" face="Arial, Helvetica, sans-serif">
        <font size="-2">Last modified
          Monday, 15-Oct-2012 14:55:23 PDT </font>
      </font></div></td>
    </tr>
    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script src="https://www.google-analytics.com/ga.js" type="text/javascript"></script>
    <script type="text/javascript">
      try {
        var pageTracker = _gat._getTracker("UA-8834111-1");
        pageTracker._trackPageview();
      } catch(err) {}
    </script>
    </tbody>
  </table>
</span>

<table cellspacing="0" cellpadding="0" style="width: 130px; display: none; top: 18px; position: absolute; left: 1120px; " class="gstl_0 gssb_c"><tbody><tr><td class="gssb_f"></td><td class="gssb_e" style="width: 100%; "></td></tr></tbody></table>
