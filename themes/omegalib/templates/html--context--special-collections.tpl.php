<!DOCTYPE html>

<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Special Collections | University Libraries</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php print $theme_path; ?>/css/universitylibraries/css/normalize.css">
  <link rel="stylesheet" href="<?php print $theme_path; ?>/css/grid/omegalib_gridpak/normal/omegalib-gridpak-normal-24.css">
  <link rel="stylesheet" href="<?php print $theme_path; ?>/css/universitylibraries/css/select2.css">
  <link rel="stylesheet" href="<?php print $theme_path; ?>/css/universitylibraries/css/universitylibraries.css">
  <link rel="stylesheet" href="<?php print $theme_path; ?>/css/universitylibraries/speccol/css/speccol.css">
  <?php print $styles; ?>
<!--  <script src="js/modernizr-2.6.2.min.js"></script> -->
  <!--[if lt IE 9]>
    <script src="../../../js/universitylibraries/js/html5shiv.js"></script>
  <![endif]-->
</head>
<body class="front speccol">
  <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
  <div id="global-header-bar" class="wrapper">
    <div class="container">
      <div class="row">
        <div class="col span_6 unlv-logo">
          <a href="http://www.unlv.edu" alt="UNLV Home">
            <img src="<?php print $theme_path; ?>/images/logo-unlv.png">
          </a>
        </div>
        <div class="col span_18 unlv-links">
          <ul>
            <li>
              <a href="https://webcampus.unlv.edu/">WebCampus</a>
            </li>
            <li>
              <a href="https://my.unlv.nevada.edu/psp/lvporprd/EMPLOYEE/EMPL/h/?tab=PAPP_GUEST">MyUNLV</a>
            </li>
            <li>
              <a href="http://rebelmail.unlv.edu/">Rebelmail</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <section class="header wrapper">
    <div class="container">
      <div class="row">
        <div class="col span_24">
          <a class="logo" href="<?php print base_path(); ?>speccol">
            <img id="logo" src="<?php print $theme_path; ?>/css/universitylibraries/speccol/img/logo-special-collections.png" title="Special Collections">
          </a>
          <div class="col span_12">
            <?php $block = module_invoke('libsearch', 'block_view', 'library_search_bar_speccol'); print $block['content']; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <nav class="wrapper main-nav">
    <div class="container">
      <div class="row">
        <div class="col span_24">
          <div class="mobile-menu">
            <button>
              <span class="glyph">&#xf0c9;</span> Menu
            </button>
          </div>
          <?php print $menu_main_speccol; ?>
        </div>
      </div>
    </div>
  </nav>
  <?php // print $page_top; ?>
  <?php print $page; ?>
  <?php // print $page_bottom; ?>
  <footer>
    <div class="container">
      <div class="row">
        <!-- <div class="col span_6"> -->
          <?php $block = module_invoke('library_content', 'block_view', 'footer_locations_speccol'); print $block['content']; ?>
        <!-- </div> -->
      </div>
    </div>
  </footer>
<!--
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>
-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYDjRnOthPllmhXTQJi8UCXO3JojoaqDM&sensor=false"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script src="<?php print $theme_path; ?>/js/universitylibraries/select2.min.js"></script>
  <script src="<?php print $theme_path; ?>/js/universitylibraries/speccol.js"></script>
  <script src="<?php print $theme_path; ?>/js/universitylibraries/jquery.easyfader.js"></script>
  <script src="<?php print $theme_path; ?>/js/universitylibraries/respond.min.js"></script>
  <script src="<?php print $theme_path; ?>/js/universitylibraries/universitylibraries.js"></script>
  <!-- <script src="../../../js/universitylibraries/jquery.touchSwipe.min.js"></script> -->
    <script type="text/javascript">
    
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
      var map;
      // Basic options for a simple Google Map
      // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
      var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 17,
        zoomControl:  true,
        // zoomControlStyle: small,
        scrollwheel: false,
        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(36.1072,-115.142325), 
        // How you would like to style the map. 
        streetViewControl: false,
        mapTypeControl: false,
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{"featureType":"all","stylers":[{"saturation":-100},{"gamma":0.5}]}]
      };
      // Get the HTML DOM element that will contain your map 
      // We are using a div with id="map" seen below in the <body>
      var mapElement = document.getElementById('map');
      // Create the Google Map using out element and options defined above
      var map = new google.maps.Map(mapElement, mapOptions);
      // move map when needed
      // $('select#liblocation').live('change', function(e) {
      $('select#liblocation').change(function(e) {
        $('.lib-branch.active').hide().removeClass('active');
        $('#lib-'+ e.val).fadeIn().addClass('active');
        switch (e.val) {
          case 'specialcol':
            moveToLocation(36.1072,-115.142325);
            $('div#lib-'+ e.val +'-hours').fadeIn().addClass('active');
            $('div.hours').fadeIn();
            break;
          case 'lied':
            moveToLocation(36.1072,-115.142325);
            $('div#lib-'+ e.val +'-hours').fadeIn().addClass('active');
            $('div.hours').fadeIn();
            break;
          case 'arch':
            moveToLocation(36.102571,-115.138773);
            $('div#lib-'+ e.val +'-hours').fadeIn().addClass('active');
            $('div.hours').fadeIn();
            break;
          case 'curr':
            moveToLocation(36.109294,-115.140635);
            $('div#lib-'+ e.val +'-hours').fadeIn().addClass('active');
            $('div.hours').fadeIn();
            break;
          case 'music':
            moveToLocation(36.110596,-115.138191);
            $('div#lib-'+ e.val +'-hours').fadeIn().addClass('active');
            $('div.hours').fadeIn();
            break;
          case 'law':
            moveToLocation(36.107952,-115.139975);
            // remove hours altogether
            $('div.hours').fadeOut();
            break;
        }
      });

      // funtion that moves the map
      function moveToLocation(lat, lng) {
        var center = new google.maps.LatLng(lat, lng);
        // using global variable:
        map.panTo(center);
      }
    }
  </script>
  <?php print $scripts; ?>
</body>
</html>
