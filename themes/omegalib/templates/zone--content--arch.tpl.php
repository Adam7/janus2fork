<?php if ($wrapper): ?><div<?php print $attributes; ?>><?php endif; ?>  
  <div<?php print $content_attributes; ?>>       
    <?php if ($messages): ?>
      <div id="messages" class="grid-<?php print $columns; ?>"><?php print $messages; ?></div>
    <?php endif; ?>
    <?php if ($zone == 'content' && $title != 'none'): ?>
      <div class="col span_24 bread grid-24">
        <?php if ($breadcrumb != 'none'): ?>
          <div id="breadcrumb">
            <?php print $breadcrumb; ?>
          </div>
        <?php endif; ?> 
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      </div>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
<?php if ($wrapper): ?></div><?php endif; ?>