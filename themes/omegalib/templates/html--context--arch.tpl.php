<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width" />
    <title>Architecture Studies Library | University Libraries</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width">
    <link href="/img/favicon.ico" rel="shortcut icon" />
    <?php print $styles; ?> 
<!--     
    <link rel="stylesheet" media="all" type="text/css" href="<?php // print $theme_path;?>/css/universitylibraries/css/gridpack.css"/>   
    <link rel="stylesheet" media="all" type="text/css" href="../_css/universitylibraries.css"/>
    <link rel="stylesheet" media="all" type="text/css" href="_css/asl.css"/>
-->    
    <link href="<?php print $theme_path;?>/css/universitylibraries/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
  </head>
  
  <body<?php print $attributes;?>>
    <div id="global-header-bar">
      <div class="page">
        <div class="row">
          <div class="col span_6">
            <a href="http://www.unlv.edu" alt="UNLV Home">
            <img src="<?php print $theme_path; ?>/images/logo-unlv.png">
            </a>
          </div>
          <div class="col span_18">
            <ul>
              <li>
                <a href="https://webcampus.unlv.edu/">WebCampus</a>
              </li>
              <li>
                <a href="https://my.unlv.nevada.edu/psp/lvporprd/EMPLOYEE/EMPL/h/?tab=PAPP_GUEST">MyUNLV</a>
              </li>
              <li>
                <a href="http://rebelmail.unlv.edu/">Rebelmail</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="header">
      <div class="page header">
        <div class="row">
          <div class="col span_24">
            <a class="logo" href="/arch">
            <img id="logo" src="<?php print $theme_path; ?>/css/universitylibraries/asl/img/logo-architecture-studies-library.png" title="Architecture Studies Library Home">
            </a>
          </div>
        </div>
      </div>
      <div id="nav">
        <div class="page">
          <div class="row">
            <div class="col span_24">
              <?php print $menu_main_arch; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php print $page; ?>

    <div id="footer">
      <div class="page">
        <div class="row">
          <div class="col span_6">
            <h2>Locations</h2>
            <div class="locations">
              <div class="select-library stilts">
                <select name="liblocation" id="liblocation" tabindex="1">
                  <option value="arch">
                    Architecture Library
                  </option>
                  <option value="lied">
                    Lied Library
                  </option>
                  <option value="speccol">
                    Special Collections
                  </option>
                  <option value="curr">
                    Curriculum Materials
                  </option>
                  <option value="music">
                    Music Library
                  </option>
                  <option value="law">
                    Law Library
                  </option>
                </select>
              </div>
            </div>
            <div id="lib-arch" class="lib-branch active">
              <p class="address">
              4505 South Maryland Pkwy.
              <br>
              Box 454049
              <br>
              Las Vegas, Nevada
              <br>
              89154-4049
              </p>
              <p>
              <a href="/arch">Architecture Library</a>
              <br>
              (702) 895-1959
              </p>
            </div>
            <div id="lib-lied" class="lib-branch">
              <p class="address">
              4505 South Maryland Parkway
              <br>
              Box 457001
              <br>
              Las Vegas, Nevada
              <br>
              89154-7001
              <br>
              <br>
              (702) 895-2111
              </p>
            </div>
            <div id="lib-speccol" class="lib-branch">
              <p class="address">
              4505 South Maryland Parkway
              <br>
              Box 457010
              <br>
              Las Vegas, Nevada
              <br>
              89154-7010
              <br>
              <br>
              <a href="/speccol">Special Collections</a>
              <br>
              (702) 895-2234
              </p>
            </div>
            <div id="lib-curr" class="lib-branch">
              <p class="address">
              4505 South Maryland Parkway
              <br>
              Box 453009
              <br>
              Las Vegas, Nevada
              <br>
              89154-3009
              <br>
              <br>
              <a href="/cml">Curriculum Materials Library</a>
              <br>
              (702) 895-3593
              </p>
            </div>
            <div id="lib-music" class="lib-branch">
              <p class="address">
              4505 South Maryland Parkway
              <br>
              Box 457002
              <br>
              Las Vegas, Nevada
              <br>
              89154-7002
              <br>
              <br>
              <a href="/music">Music Library</a>
              <br>
              (702) 895-2541
              </p>
            </div>
            <div id="lib-law" class="lib-branch">
              <p class="address">
              4505 South Maryland Parkway
              <br>
              Box 451003
              <br>
              Las Vegas, Nevada
              <br>
              89154-1003
              <br>
              <br>
              <a href="http://law.unlv.edu/law-library/home.html">Law Library</a>
              <br>
              (702) 895-3671
              </p>
            </div>
            <h5 class="todays-hours">Today&#39;s Hours:</h5>
            <?php include_once ("library_content-hours.inc");
              PrintBranchHour_Today (7);
            ?>
          </div>
          <div class="col span_6">
            <h2 class="placeholder">
            &nbsp;
            </h2>
            <div class="map map-arch"></div>
            <div class="footer-block">
              <h5>
              More Information
              </h5>
              <ul class="menu no-bullet stilts">
                <li>
                  <a href="/services/hours">Future Library Hours</a>
                </li>
                <li>
                  <a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a>
                </li>
                <li>
                  <a href="http://www.unlv.edu/parking">Parking Information</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col span_6">
            <h2>Contact</h2>
            <div class="footer-block">
              <h5>Ask Us!</h5>
              <ul class="menu no-bullet">
                <li>
                  <a href="#" onclick="Chat=window.open('https://v2.libanswers.com/widget_standalone.php?hash=2de07cac2d72945bcdf47593b1c7308d','Chat','toolbar=no,location=no,directories=no,status, menubar=no,scrollbars,resizable,width=350,height=700'); return false;">
                  <span class="glyph">&#xf086;</span> Chat
                  </a>
                </li>
                <li>
                  <span class="footer-text">
                  <span class="glyph">&#xf10a;</span> Text: (702) 945-0822
                  </span>
                </li>
                <li>
                  <span class="footer-text">
                  <span class="glyph">&#xf095;</span> Phone: (702) 895-2111
                  </span>
                </li>
                <li>
                  <a href="/ask/email.php">
                  <span class="glyph">&#xf0e0;</span> Email
                  </a>
                </li>
              </ul>
            </div>
            <div class="footer-block">
              <h5>Social Media</h5>
              <div class="contact-group-links ">
                <ul class="menu no-bullet">
                  <li>
                    <a href="https://www.facebook.com/unlvlib">
                    <span class="glyph facebook">&#xf082;</span> Facebook
                    </a>
                  </li>
                  <li>
                    <a href="https://twitter.com/unlvlibraries">
                    <span class="glyph twitter">&#xf099;</span> Twitter
                    </a>
                  </li>
                  <li>
                    <a href="http://www.youtube.com/user/unlvlibraries">
                    <span class="glyph youtube">&#xf04b;</span> YouTube
                    </a>
                  </li>
                    <li>
                        <a href="http://instagram.com/unlvlibraries">
                        <span class="glyph instagram">&#xf16d;</span> Instagram
                        </a>
                    </li>
                  <li>
                    <a href="/contact/social_media">All Social Media</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col span_6">
            <h2 class="placeholder">
            &nbsp;
            </h2>
            <div class="footer-block">
              <h5>
              Directories
              </h5>
              <ul class="menu no-bullet">
                <li>
                  <a href="/contact/librarians_by_subject">Librarians by Subject</a>
                </li>
                <li>
                  <a href="/about/staff/libstafinfo.php?style=2">Staff Directory</a>
                </li>
              </ul>
            </div>
            <div class="footer-block">
              <h5>
              Employment
              </h5>
              <ul class="menu no-bullet">
                <li>
                  <a href="/employment">Full-Time</a>
                </li>
                <li>
                  <a href="/employment/student.html">Student</a>
                </li>
              </ul>
            </div>
            <div class="footer-block">
              <h5>
              Feedback
              </h5>
              <ul class="menu no-bullet">
                <li>
                  <a href="/comments.php"><span class="glyph">&#xf0a1;</span> Provide Feedback</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col span_24">
            <?php print $user_link; ?>
          </div>
        </div>
      </div>
    </div>
    <?php print $scripts; ?>
<!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
    <script type="text/javascript" src="../_js/jquery.selectbox-0.2.js"></script>
    <script type="text/javascript" src="../_js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="../_js/universitylibraries.js"></script>
    <script type="text/javascript" src="../_js/respond.min.js"></script> 
-->
  </body>
</html>
