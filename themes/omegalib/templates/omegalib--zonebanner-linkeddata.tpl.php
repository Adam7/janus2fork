<div id="ld-header">
  <?php print theme('image', array('path' => path_to_theme().'/images/UNLV-LDBanner-Large-No-Image.png', 'attributes' => array('class' => 'ld-logo'))); ?>
  <div id="ld-spacer" class="grid-16">&nbsp;</div>
  <div id="ld-peeps" class="grid-8">
    <div id="cory" class="grid-4">
      <?php print theme('image', array('path' => path_to_theme().'/images/lampert-cory.jpg', 'attributes' => array('class' => 'ld-photo'))); ?>
      <div class="name"><a href="http://works.bepress.com/corylampert" target="_blank">Cory Lampert</a></div>
      <div class="position">Head of Digiatal Collections</div>
      <div class="email"><a href="mailto:cory.lampert@unlv.edu">cory.lampert@unlv.edu</a></div>
    </div>
    <div id="silvia" class="alpha grid-4 omega">
      <?php print theme('image', array('path' => path_to_theme().'/images/southwick-silvia.jpg', 'attributes' => array('class' => 'ld-photo'))); ?>
      <div class="name"><a href="http://works.bepress.com/corylampert" target="_blank">Silvia Southwick</a></div>
      <div class="position">Digital Collections Metadata Librarian</div>
      <div class="email"><a href="mailto:silvia.southwick@unlv.edu">silvia.southwick@unlv.edu</a></div>
    </div>
  </div>
</div>