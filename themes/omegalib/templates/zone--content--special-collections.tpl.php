<?php if ($messages): ?>
	<div class="row">
		<div class="span_24">
    	<div id="messages"><?php print $messages; ?></div>
   	</div>
	</div>
<?php endif; ?>
<div class="row">
	<?php print $content; ?>
</div>