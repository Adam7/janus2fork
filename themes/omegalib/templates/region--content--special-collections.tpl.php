<div class="col span_16 main">  
  <div<?php print $attributes; ?>>
    <div<?php print $content_attributes; ?>>
      <?php if ($tabs && !empty($tabs['#primary'])): ?>
        <div class="tabs clearfix"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php print $content; ?>
    </div>
  </div>
</div>