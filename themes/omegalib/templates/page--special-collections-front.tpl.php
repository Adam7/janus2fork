<section class="content home">
  <div class="container">
    <div class="row">
      <div class="col span_24">
        <div class="breadcrumb">
          <a href="<?php print base_path(); ?>">University Libraries</a> 
        </div>
        <div>Discover rare or unique research materials in Special Collections about Las Vegas, Southern Nevada and gaming.</div>
      </div>
    </div>
    <div class="row">
      <?php print render($page['content']); ?>
    </div>
    <div class="row">
      <div class="col span_8">
        <h4>About</h4>
        <?php print $sub_menus['sub_menu_about']; ?>
      </div>
      <div class="col span_8">
        <h4>Research &amp; Services</h4>
        <?php print $sub_menus['sub_menu_researchservices']; ?>
      </div>
      <div class="col span_8">
        <h4>Visit</h4>
        <?php print $sub_menus['sub_menu_visit']; ?>
      </div>
    </div>
    <div class="row">
      <div class="col span_8">
        <h4>Centers</h4>
        <?php print $sub_menus['sub_menu_centers']; ?>
      </div>
      <div class="col span_8">
        <h4>Collections</h4>
        <?php print $sub_menus['sub_menu_collections']; ?>
      </div>
      <div class="col span_8">
        <h4>Collecting Strengths</h4>
        <?php print $sub_menus['sub_menu_collectingstrengths']; ?>
      </div>
    </div>
  </div>
</section>
<section class="prefooter">
  <?php print render($footer_slide['content']); ?>
</section>
