<?php

/**
 * @file
 * Default view template to display a item in an RSS feed.
 *
 * @ingroup views_templates
 */
//print_r($item_elements);
//print_r(get_defined_vars());
//print_r($view);

?>
<?php if ($node->type == 'blog'): ?>
  <?php if ($node->field_blog_name['und'][0]['tid'] == '37'): ?>
    <?php //print_r($node); ?>
    <item>
      <title><?php print $title; ?></title>
      <itunes:summary><?php print strip_tags($node->body['und'][0]['safe_value']); ?></itunes:summary>
      <?php if(array_key_exists('und', $node->field_file_pub)): ?>
        <enclosure url="<?php print file_create_url($node->field_file_pub['und'][0]['uri']); ?>" length="<?php print $node->field_file_pub['und'][0]['filesize']; ?>" type="audio/mpeg" />
      <?php endif; ?>
      <guid>http://www.library.unlv.edu/node/<?php print $node->nid; ?></guid>
      <pubDate><?php print $node->rss_elements[0]['value']; ?></pubDate>
      <!-- <itunes:duration><?php //TODO: print $node->duration; ?></itunes:duration> -->
      <?php if (isset($tags)): ?>
        <itunes:keywords><?php print implode(", ", $tags); ?></itunes:keywords>
      <?php endif; ?>
      <link><?php print $link; ?></link>
      <description><?php print strip_tags($node->body['und'][0]['safe_value']); ?></description>
      <?php // print $item_elements; ?>
    </item>
  <?php endif; ?>
<?php else: ?>
  <item>
    <title><?php print $title; ?></title>
    <link><?php print $link; ?></link>
    <description><?php print $description; ?></description>
    <?php print $item_elements; ?>
  </item>
<?php endif; ?>
