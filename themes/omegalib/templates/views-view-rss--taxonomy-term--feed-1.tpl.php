<?php
/**
 * @file
 * Default template for feed displays that use the RSS style.
 *
 * @ingroup views_templates
 *
 * Includes a hardcoded template for the Center for Gaming Research Podcast
 */
?>
<?php if($variables['view']->args[0] == 37 && $variables['view']->args[1] == 863): ?>
<?php print "<?xml"; ?> version="1.0" encoding="utf-8" <?php print "?>"; ?>
    <rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
      <channel>
        <title>UNLV Gaming Podcast</title>
        <link>http://www.library.unlv.edu/center_for_gaming_research/podcast</link>
        <description>Lectures from the Gaming Research Colloquium Series and interviews with gaming authors, industry figures, and researchers.</description>
        <language>en</language>
        <copyright>Copyright <?php print date("Y"); ?></copyright>
        <itunes:subtitle></itunes:subtitle>
        <itunes:author>UNLV Libraries</itunes:author>
        <itunes:summary>Lectures from the Gaming Research Colloquium Series and interviews with gaming authors, industry figures, and researchers.</itunes:summary>
        <itunes:owner>
          <itunes:name>David G. Schwartz</itunes:name>
          <itunes:email>dgs@unlv.nevada.edu </itunes:email>
        </itunes:owner>
        <itunes:image href="http://gaming.unlv.edu/audio/upg_icon.jpg" />
<?php print $items; ?>
      </channel>
    </rss>
<?php else: ?>
  <?php print "<?xml"; ?> version="1.0" encoding="utf-8" <?php print "?>"; ?>
  <rss version="2.0" xml:base="<?php print $link; ?>"<?php print $namespaces; ?>>
    <channel>
      <title><?php print $title; ?></title>
      <link><?php print $link; ?></link>
      <description><?php print $description; ?></description>
      <language><?php print $langcode; ?></language>
      <?php print $channel_elements; ?>
      <?php print $items; ?>
    </channel>
  </rss>
<?php endif; ?>
