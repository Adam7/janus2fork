<!-- Content
		============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">

      <div class="container clearfix">


<?php //print $messages; ?>


        <br>
        <h4 class="center">New Arrivals</h4>

        <div class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="4" data-items-xs="5" data-items-sm="6" data-items-md="8" data-items-lg="9" data-autoplay="4000" data-loop="true">

          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book.jpg" alt="Image 1"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book2.jpg" alt="Image 2"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book3.jpg" alt="Image 3"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book4.jpg" alt="Image 4"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book5.jpg" alt="Image 5"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book6.jpg" alt="Image 6"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book7.jpg" alt="Image 7"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book8.jpg" alt="Image 8"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book9.jpg" alt="Image 9"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book10.jpg" alt="Image 10"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book5.jpg" alt="Image 11"></a>
          </div>
          <div class="oc-item">
            <a href="#"><img src="<?php print $theme_path; ?>/images/book8.jpg" alt="Image 12"></a>
          </div>
        </div>


      </div>

    </div>







    <div class="container clearfix">

      <div class="row">

        <div class="col-md-6 bottommargin">

          <div class="col_full bottommargin-lg clearfix">

            <div class="fancy-title title-border">
              <h3>News</h3>
            </div>

            <div class="ipost clearfix">
              <div class="col_one_third bottommargin-sm">
                <div class="entry-image">
                  <a href="#"><img class="image_fade" src="<?php print $theme_path; ?>/images/hp.jpg" alt="Image"></a>
                </div>
              </div>
              <div class="col_two_third col_last bottommargin-sm col_last">
                <div class="entry-title">
                  <h3><a href="blog-single.html">This is a blog post about new books that are coming to the TDRL soon</a></h3>
                </div>
                <ul class="entry-meta clearfix">
                  <li><i class="icon-calendar3"></i> 10th Jun 2016</li>
                  <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 21</a></li>
                  <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                </ul>
                <div class="entry-content">
                  <p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, repudiandae. Asperiores,
                    tenetur, blanditiis, quaerat odit ex exercit ationem veritatis laboriosam esse perferendis.</p>
                </div>
              </div>
            </div>

            <div class="clear"></div>



            <div class=" col_full nobottommargin col_last">

              <div class="spost clearfix">
                <div class="entry-image">
                  <a href="#"><img src="<?php print $theme_path; ?>/images/s3.jpg" alt=""></a>
                </div>
                <div class="entry-c">
                  <div class="entry-title">
                    <h4><a href="#">New books coming in July</a></h4>
                  </div>
                  <ul class="entry-meta">
                    <li><i class="icon-calendar3"></i> 26th May 2016</li>
                    <li><a href="#"><i class="icon-comments"></i> 7</a></li>
                  </ul>
                </div>
              </div>

              <div class="spost clearfix">
                <div class="entry-image">
                  <a href="#"><img src="<?php print $theme_path; ?>/images/s4.jpg" alt=""></a>
                </div>
                <div class="entry-c">
                  <div class="entry-title">
                    <h4><a href="#">Celebrity author visiting soon</a></h4>
                  </div>
                  <ul class="entry-meta">
                    <li><i class="icon-calendar3"></i> 25th May 2016</li>
                    <li><a href="#"><i class="icon-comments"></i> 13</a></li>
                  </ul>
                </div>
              </div>

            </div>

          </div>






          <!-- facebook widget -->
          <div class="widget clearfix">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FUNLVCurriculumMaterialsLibrary&amp;width=360&amp;height=240&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=499481203443583"
            scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:360px; height:240px; max-width: 100% !important;" allowTransparency="true"></iframe>
          </div>
          <!-- end facebook widget -->

        </div>


        <!-- Second Column Start -->
        <div class="col-md-6 col-sm-12 no-gutter">

          <div class="line hidden-lg hidden-md"></div>








          <div class="fancy-title title-border">
            <h3>FAQS</h3>
          </div>

          <div class="col-md-7">
            <div class="r-margin">
              <div class="accordion clearfix" data-active="1">

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>How do I view articles from home or off-campus?</div>
                <div class="acc_content clearfix">When prompted, use either your <a href="http://oit.unlv.edu/accounts/active-directory">ACE account</a> OR your <a href="https://www.library.unlv.edu/help/activating_your_account">activated RebelCard barcode and PIN </a>to login.
                  <br> <a href="http://ask.library.unlv.edu/faq/40676">More <i class="icon-line-arrow-right"></i></a></div>

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>How do I find peer reviewed or scholarly articles?</div>
                <div class="acc_content clearfix"><a href="http://ask.library.unlv.edu/faq/42921">This tutorial </a>is available to help you! Additional help is available if you need to <a href="http://ask.library.unlv.edu/">access journal article from off-campus.</a></div>

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Where can I get help with my ACE Account?</div>
                <div class="acc_content clearfix">For help with your ACE account, RebelMail, WebCampus or Secure Wireless, please contact the campus OIT help desk at 702-895-0777 or visit them at CBC B113 (during regular hours).
                  <br><a href="http://ask.library.unlv.edu/faq/56515">More <i class="icon-line-arrow-right"></i></a></div>

              </div>

            </div>
          </div>

          <div class="col-md-5 widget_links bottommargin-sm">



            <ul>
              <li><a href="#">Professional Development Collection</a></li>
              <li><a href="#">Picture Books</a></li>
              <li><a href="#">Juvenile Fiction </a></li>
              <li><a href="#">Graphic Novels</a></li>
            </ul>


          </div>


          <br>



          <!-- Slider -->

          <div class="col-md-12">
            <div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-arrows="true" data-thumbs="true">
              <div class="flexslider">
                <div class="slider-wrap">
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t2.jpg">
                    <a href="#">
                      <img src="<?php print $theme_path; ?>/images/2.jpg" alt="">
                      <div class="overlay">
                        <div class="text-overlay">
                          <div class="text-overlay-title">
                            <h3>Slide</h3>
                          </div>
                          <div class="text-overlay-meta">
                            <span>One</span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t1.jpg">
                    <a href="#">
                      <img src="<?php print $theme_path; ?>/images/1.jpg" alt="">
                      <div class="overlay">
                        <div class="text-overlay">
                          <div class="text-overlay-title">
                            <h3>Slide</h3>
                          </div>
                          <div class="text-overlay-meta">
                            <span>Two</span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t3.jpg">
                    <a href="#">
                      <img src="<?php print $theme_path; ?>/images/3.jpg" alt="">
                      <div class="overlay">
                        <div class="text-overlay">
                          <div class="text-overlay-title">
                            <h3>Slide</h3>
                          </div>
                          <div class="text-overlay-meta">
                            <span>Three</span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t4.jpg">
                    <iframe src="http://player.vimeo.com/video/137925493" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </div>
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t5.jpg">
                    <a href="#">
                      <img src="<?php print $theme_path; ?>/images/5.jpg" alt="">
                      <div class="overlay">
                        <div class="text-overlay">
                          <div class="text-overlay-title">
                            <h3>Slide</h3>
                          </div>
                          <div class="text-overlay-meta">
                            <span>Five</span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="slide" data-thumb="<?php print $theme_path; ?>/images/t6.jpg">
                    <a href="#">
                      <img src="<?php print $theme_path; ?>/images/6.jpg" alt="">
                      <div class="overlay">
                        <div class="text-overlay">
                          <div class="text-overlay-title">
                            <h3>Slide</h3>
                          </div>
                          <div class="text-overlay-meta">
                            <span>Six</span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Slider End -->







        </div>

      </div>

    </div>

  </div>

</section>
<!-- #content end -->
