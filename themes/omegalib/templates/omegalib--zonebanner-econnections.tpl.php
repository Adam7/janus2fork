<div id="econnections-header" class="alpha grid-24 omega">
  <?php print l(theme('image', array('path' => path_to_theme().'/images/econ/econ-logo.png', 'attributes' => array('class' => 'econlogo'),)), 'econnections', array('html' => TRUE)); ?>
  <div class="econrand" id="econ-rand-<?php print rand(1, 4); ?>"></div>
  <?php print theme('image', array('path' => path_to_theme().'/images/econ/econ-shadow.png', 'attributes' => array('class' => 'econshadow'))); ?>
</div>
<div id="econnections-tagline">
  <?php
    print theme('image', array('path' => path_to_theme().'/images/econ/econ-tagline-unlv.jpg', 'attributes' => array('class' => 'econtaglineunlv')));
    print theme('image', array('path' => path_to_theme().'/images/econ/econ-tagline-tagline.jpg', 'attributes' => array('class' => 'econtagline')));
  ?>
</div>
 