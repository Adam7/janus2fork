<div class="col span_12 main">    
  <?php if ($tabs && !empty($tabs['#primary'])): ?>
    <div class="tabs clearfix"><?php print render($tabs); ?></div>
  <?php endif; ?>
  <?php print $content; ?>
</div>