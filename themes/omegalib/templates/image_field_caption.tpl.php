<?php
/**
 * $image - contains the image html rendered by Drupal
 * $caption - contains the image field caption string
 */
?>
<?php print $image; ?>
<p class="caption">
  <?php print $caption; ?>
</p>
