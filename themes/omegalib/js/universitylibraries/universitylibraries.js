(function($){
function searchSelect() {
  $('ul.sb_dropdown > li ul li a,' +
    'ul.sb_dropdown input').click(function(e) {
    // prevent default for <a> tags
    if (e.target.localName == 'a') {
      e.preventDefault();
    }
    // remove active classes for both <a> & <input> contexts
    $('ul.sb_dropdown > li ul li a,' +
      'ul.sb_dropdown input').removeClass('active');
    // add active class to the clicked item
    $(this).addClass('active');
    $("ul.sb_dropdown .search-specific input").prop('disabled', true);
    // Quick Search
    if ($('ul.sb_dropdown > li.quick-search ul li a,' +
          'ul.sb_dropdown input#r1').hasClass('active')){
      $("li.quick-search .search-specific input").prop('disabled', false);
      $('li.quick-search .search-specific input#' + $(this).attr('data-value')).attr('checked', true); 
      $('#ui_element').attr('action', $('li.quick-search').attr('data-value'));
      $('.sb_input').attr('name', 's.q');
      $('input.opt').remove();
    // Catalog
    } else if ($('ul.sb_dropdown > li.catalog-search ul li a,' +
                 'ul.sb_dropdown input#r2').hasClass('active')) {
      $('#ui_element').attr('action', $(this).attr('data-value'));
      $('.sb_input').attr('name', 'search');
      $('input.opt').remove();
    // Site search
    } else if ($('ul.sb_dropdown > li.site-search ul li a,' +
                 'ul.sb_dropdown input#r6').hasClass('active')) {
      $("li.site-search .search-specific input").prop('disabled', false);
      $('#ui_element').attr('action', $('li.site-search').attr('data-value'));
      $('#ui_element').attr('action', $(this).attr('data-value'));
      // $('.sb_input').attr('name', $(this).attr('name'));
      $('.sb_input').attr('name', 'q');
      $('input.opt').remove();
      // add q param for site search
      $('#ui_element input.sb_search').html('<input class="opt" name="cx" value="001581898298350756962:-ch-cv_3h1k">' +
                                            '<input class="opt" name="cof" value="FORID:10">' +
                                            '<input class="opt" name="ie" value="UTF-8">');
    // Speccol Database
    } else if ($('ul.sb_dropdown > li.speccol-database-search ul li a,' +
                 'ul.sb_dropdown input#r3').hasClass('active')) {
      $('#ui_element').attr('action', $(this).attr('data-value'));
      $('.sb_input').attr('name', 'search_query');
      $('input.opt').remove();
      // adds act=2 param to activate database search
      $('#ui_element input.sb_search').html('<input class="opt" type="hidden" name="act" value="2">');
    // Speccol Database Manuscripts
    } else if ($('ul.sb_dropdown > li.speccol-manuscript-guides-search ul li a,' +
                 'ul.sb_dropdown input#r4').hasClass('active')) {
      $('#ui_element').attr('action', $(this).attr('data-value'));
      $('.sb_input').attr('name', 'q');
      $('input.opt').remove();
    // Digital Collections
    } else if ($('ul.sb_dropdown > li.speccol-digital-collections-search ul li a,' +
                 'ul.sb_dropdown input#r5').hasClass('active')) {
      $('#ui_element').attr('action', $(this).attr('data-value'));
      $('.sb_input').attr('name', 'searchterm');
      $('input.opt').remove();
    }
  });
}

function forcedDropDown() {
  $(function() {
    // the element
    var $ui = $('#ui_element');
    // focus event listener for searchbar
    $('.sb_input').focus(function(event) {
      downup('sb_down');
    });
    // click event for the arrow button in serchbar
    $('#sb_arrow').click(function(event) {
      downup(event.target.className);
    });
    // blur event for when searchbar's input loses focus
    $('.sb_input').blur(function(event) {
      $('body').click(function(event) {
        if ($(event.target).parents('.sb_wrapper').length) {
          // do nothing
        } else {
          downup('sb_up');
        }
      });
    });

    // flip searchbar down or up
    function downup(flip) {
      var pilf = 'sb_up';
      if (flip == 'sb_up') { pilf = 'sb_down'; }
      if ($('#sb_arrow').hasClass(flip)) {
        $('.sb_dropdown').slideToggle(100);
        $('#sb_arrow').removeClass(flip);
        $('#sb_arrow').addClass(pilf);
      } 
    }

    // selecting all checkboxes
    $ui.find('.sb_dropdown').find('label[for="all"]').prev().bind('click',function(){
        $(this).parent().siblings().find(':checkbox').attr('checked',this.checked).attr('disabled',this.checked);
    });
  });
}

function ddm() {
  $('html').click(function() {
    $('.ddm ul.drop').hide();
    $('.ddm .selectbox').removeClass('focus');
  });
  $('.ddm .selectbox').on('click', function(e) {
    e.stopPropagation();
    $('.ddm .selectbox').toggleClass('focus');
    $('.ddm ul.drop').slideToggle(100);
  });
  $('.ddm ul.drop li ul li a').on('click', function(e) {
    $('.ddm ul.drop li ul li a').removeClass('active');
    $('.ddm span.chosen').text($(this).text());
    $(this).addClass('active');
    $('.ddm ul.drop').hide();
    $("ul.drop .search-specific input").prop('disabled', true);
    if ($('ul.drop > li.quick-search ul li a').hasClass('active')){
      $("li.quick-search .search-specific input").prop('disabled', false);
      $('li.quick-search .search-specific input#' + $(this).attr('data-value')).attr('checked', true); 
      $('#ui_element').attr('action', $('li.quick-search').attr('data-value'));
      $('.sb_input').attr('name', 's.q');
    } else if ($('ul.drop > li.catalog-search ul li a').hasClass('active')) {
      $('#ui_element').attr('action', $(this).attr('data-value'));
      $('.sb_input').attr('name', 'search');
    } else if ($('ul.drop > li.site-search ul li a').hasClass('active')) {
      $("li.site-search .search-specific input").prop('disabled', false);
      $('#ui_element').attr('action', $('li.site-search').attr('data-value'));
      $('.sb_input').attr('name', $(this).attr('name'));
    }
  });
}

// Select2
function select2() {
  $(".useselect2-main").select2({
    minimumResultsForSearch: -1
  }); 
  $(".useselect2").select2({minimumResultsForSearch: -1}); 
  $(".useselect2-search").select2();
  $(".useselect2-search-guides").select2({
    width: 200
  });
}

$(document).ready(function() {
  searchSelect();
  forcedDropDown();
	select2();
  ddm();
});

})(jQuery);
