(function($) {

  // Define mobile behavior
  function automagicResize() {
    if ($('.mobilecheck').css('z-index') != 1 ) { 
      $('ul#main-menu').show();
    } else { 
      $('ul#main-menu').hide();
    }
  }

  // Mobile Menu button behavior
  function mobileMenu() {
    $('a.mobile-menu').on('click', function(e) {
      e.preventDefault();
      $('ul#main-menu').slideToggle(200);
    });
  }

  // Select2 plugin for styled dropdowns
  function select2() {
    $("select#searchDropdownBox, select#library-select, .region-sidebar-second select").select2({
      minimumResultsForSearch: -1
    });
  }

  $(document).ready(function() {
    automagicResize();
    mobileMenu();
    select2();
  });

  $(window).resize(function(){
    automagicResize();
  });

})(jQuery); 
