<?php

/**
 * Implements hook_preprocess_html().
 */
function omegalib_alpha_preprocess_comment(&$vars) {
  $vars['content']['field_comment_body']['#label_display'] = 'hidden';
}