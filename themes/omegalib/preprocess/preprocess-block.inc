
<?php

/**
 * Implements hook_preprocess_block().
 */
function omegalib_alpha_preprocess_block(&$vars) {
  // add moveable class to all blocks in sidebar_first
  if ($vars['elements']['#block']->region == 'sidebar_first') {
  	$vars['attributes_array']['class'][] = 'moveable';
  }
  // add block system main template based on context
  $context = context_get();
  if ( isset ($context['context']) ) {
    foreach (array_keys($context['context']) as $context_name) {
      $vars['theme_hook_suggestions'][] = "block__system__main__" . strtolower($context_name);
    }
  }
}
