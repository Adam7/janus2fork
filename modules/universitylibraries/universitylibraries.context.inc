<?php
class context_reaction_loadjqueryuiwidget extends context_reaction {
  
  function options_form($context) {
    $form = array(
      '#tree' => TRUE,
    );

    $form['loadjqueryuiwidget_template'] = array(
      '#title' => t('jQuery widgets to load:'),
      '#type' => 'checkboxes',
      '#options' => array(t('Button'),),
      '#default_value' => 'Button',
    );

    return $form;
  }

  function execute(&$vars = NULL){
    drupal_set_message(t("Hello World from a context reacation."));
  }
}