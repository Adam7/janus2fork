<?php
/**
 * @file
 * privileges.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function privileges_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function privileges_node_info() {
  $items = array(
    'privilege' => array(
      'name' => t('Privilege'),
      'base' => 'node_content',
      'description' => t('Content that shows the Library access and borrowing privileges.'),
      'has_title' => '1',
      'title_label' => t('User Group'),
      'help' => '',
    ),
  );
  return $items;
}
