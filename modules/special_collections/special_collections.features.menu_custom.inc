<?php
/**
 * @file
 * special_collections.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function special_collections_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-special-collections.
  $menus['menu-special-collections'] = array(
    'menu_name' => 'menu-special-collections',
    'title' => 'Special Collections',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Special Collections');


  return $menus;
}
