<?php
/**
 * @file
 * special_collections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function special_collections_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "delta" && $api == "delta") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function special_collections_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function special_collections_eck_bundle_info() {
  $items = array(
  'lib_featured_item_lib_featured_speccol' => array(
  'machine_name' => 'lib_featured_item_lib_featured_speccol',
  'entity_type' => 'lib_featured_item',
  'name' => 'lib_featured_speccol',
  'label' => 'lib_featured_speccol',
),
  );
  return $items;
}
