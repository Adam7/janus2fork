<?php
/**
 * @file
 * lib_permissions.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function lib_permissions_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_403';
  $strongarm->value = 'toboggan/denied';
  $export['site_403'] = $strongarm;

  return $export;
}
