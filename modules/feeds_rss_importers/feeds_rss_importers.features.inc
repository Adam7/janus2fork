<?php
/**
 * @file
 * feeds_rss_importers.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feeds_rss_importers_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feeds_rss_importers_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function feeds_rss_importers_eck_bundle_info() {
  $items = array(
  'feed_rss_pinterest' => array(
  'machine_name' => 'feed_rss_pinterest',
  'entity_type' => 'feed_rss',
  'name' => 'pinterest',
  'label' => 'pinterest',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function feeds_rss_importers_eck_entity_type_info() {
$items = array(
       'feed_rss' => array(
  'name' => 'feed_rss',
  'label' => 'feed_rss',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
  ),
),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function feeds_rss_importers_node_info() {
  $items = array(
    'feed_pinterest' => array(
      'name' => t('Feed from Pinterest'),
      'base' => 'node_content',
      'description' => t('Nodes that hold pinterest feeds URLs that trigger feeds that populate rss feed entities that get displayed in blocks.'),
      'has_title' => '1',
      'title_label' => t('Pinterest Feed Title'),
      'help' => '',
    ),
  );
  return $items;
}
