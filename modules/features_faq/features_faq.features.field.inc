<?php
/**
 * @file
 * features_faq.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function features_faq_field_default_fields() {
  $fields = array();

  // Exported field: 'node-faq-body'.
  $fields['node-faq-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'faq',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => '700',
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Answer',
      'required' => 0,
      'settings' => array(
        'display_summary' => 0,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '20',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '-4',
      ),
    ),
  );

  // Exported field: 'node-faq-field_faq_category'.
  $fields['node-faq-field_faq_category'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_faq_category',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'locked' => '0',
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'faq_categories',
            'parent' => '0',
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'faq',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'taxonomy_formatter',
          'settings' => array(
            'element_class' => '',
            'element_option' => '- None -',
            'links_option' => 1,
            'separator_option' => '; ',
            'wrapper_class' => '',
            'wrapper_option' => '- None -',
          ),
          'type' => 'taxonomy_term_reference_csv',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'taxonomy_formatter',
          'settings' => array(
            'element_class' => '',
            'element_option' => '- None -',
            'links_option' => 1,
            'separator_option' => '; ',
            'wrapper_class' => '',
            'wrapper_option' => '- None -',
          ),
          'type' => 'taxonomy_term_reference_csv',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_faq_category',
      'label' => 'Category',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '-3',
      ),
    ),
  );

  // Exported field: 'node-faq-field_faq_tags'.
  $fields['node-faq-field_faq_tags'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_faq_tags',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'locked' => '0',
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'faq_tags',
            'parent' => '0',
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'faq',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'taxonomy_formatter',
          'settings' => array(
            'element_class' => '',
            'element_option' => '- None -',
            'links_option' => 1,
            'separator_option' => '; ',
            'wrapper_class' => '',
            'wrapper_option' => '- None -',
          ),
          'type' => 'taxonomy_term_reference_csv',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'taxonomy_formatter',
          'settings' => array(
            'element_class' => '',
            'element_option' => '- None -',
            'links_option' => 1,
            'separator_option' => '; ',
            'wrapper_class' => '',
            'wrapper_option' => '- None -',
          ),
          'type' => 'taxonomy_term_reference_csv',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_faq_tags',
      'label' => 'Tags',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '-2',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Answer');
  t('Category');
  t('Tags');

  return $fields;
}
