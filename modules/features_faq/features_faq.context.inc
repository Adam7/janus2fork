<?php
/**
 * @file
 * features_faq.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function features_faq_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'faq';
  $context->description = '';
  $context->tag = 'FAQ';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'faq:page' => 'faq:page',
        'faq_categories:page_1' => 'faq_categories:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-faq-page' => array(
          'module' => 'views',
          'delta' => '-exp-faq-page',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-faq_categories-block' => array(
          'module' => 'views',
          'delta' => 'faq_categories-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('FAQ');
  $export['faq'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'faq_categories';
  $context->description = '';
  $context->tag = 'FAQ';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'faq_categories' => 'faq_categories',
        'faq_tags' => 'faq_tags',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-faq-page' => array(
          'module' => 'views',
          'delta' => '-exp-faq-page',
          'region' => 'content',
          'weight' => '-18',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-17',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('FAQ');
  $export['faq_categories'] = $context;

  return $export;
}
