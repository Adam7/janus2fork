(function ($) {
  $(document).ready(function() {
    // search view section
    // hide all answers
    $('body.context-faq.page-taxonomy-term div.field-name-body,' +
      'body.context-faq.page-taxonomy-term div.field-name-edit-node,' +
      'body.context-faq.page-taxonomy-term div.field-name-field-faq-category,' +
      'body.context-faq.page-taxonomy-term div.field-name-field-faq-tags').hide();
    // show answers on click
    $('body.context-faq h2').click(function() {
      console.log("." + $(this).parents('.views-row').attr("class").split(' ').join('.'));
      target = "." + $(this).parents('.views-row').attr("class").split(' ').join('.');
      $(target + ' .field-name-body,' +
        target + ' .field-name-edit-node,' +
        target + ' .field-name-field-faq-category,' +
        target + ' .field-name-field-faq-tags').slideToggle();
    });
    // term page section
    // hide all answers (& the read more button too)
    $('div.view.view-taxonomy-term article.node-faq .content, ' +
      'div.view.view-taxonomy-term article.node-faq li.node-readmore').hide();
    // show answers on click
    $('div.view.view-taxonomy-term article.node-faq h2').click(function() {
      targetId = "#" + $(this).parents('article').attr("id");
      $(targetId + " .content").slideToggle();
    });

    // hide the answers & tags
    $('body.speccol div.views-field-body,'+
      'body.speccol div.views-field-field-faq-tags').css('display', 'none');
    // toggle answers
    $('body.speccol span.views-field-title').click(function(e) {
      $(this).next().slideToggle(100);
      $(this).nextAll().eq(1).slideToggle(100);
      $(this).nextAll().eq(2).slideToggle(100);
    });

  });
})(jQuery);
