<?php
/**
 * @file
 * teacher_development_resources.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function teacher_development_resources_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-tdrl.
  $menus['menu-tdrl'] = array(
    'menu_name' => 'menu-tdrl',
    'title' => 'TDRL',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('TDRL');

  return $menus;
}
