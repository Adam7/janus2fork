<?php
/**
 * @file
 * teacher_development_resources.features.menu_uuid_links.inc
 */

/**
 * Implements hook_menu_default_menu_uuid_links().
 */
function teacher_development_resources_menu_default_menu_uuid_links() {
  $menu_uuid_links = array();

  // Exported menu link: 4915070a-4d60-11e6-ad8d-02499a97121f
  $menu_uuid_links['4915070a-4d60-11e6-ad8d-02499a97121f'] = array(
    'menu_name' => 'menu-tdrl',
    'link_path' => 'tdrl',
    'router_path' => 'tdrl',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '4915070a-4d60-11e6-ad8d-02499a97121f',
  );
  // Exported menu link: 491508ae-4d60-11e6-ad8d-02499a97121f
  $menu_uuid_links['491508ae-4d60-11e6-ad8d-02499a97121f'] = array(
    'menu_name' => 'menu-tdrl',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Research',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '491508ae-4d60-11e6-ad8d-02499a97121f',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Home');
  t('Research');


  return $menu_uuid_links;
}
