<?php
/**
 * @file
 * teacher_development_resources.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function teacher_development_resources_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Teacher Development & Research Library',
    'description' => '',
    'format' => 'html_full',
    'weight' => 0,
    'uuid' => '5bef8464-c5a4-4d5c-9371-949ebc0f712c',
    'vocabulary_machine_name' => 'page_category',
  );
  return $terms;
}
