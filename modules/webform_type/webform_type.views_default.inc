<?php
/**
 * @file
 * webform_type.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function webform_type_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'web_forms';
  $view->description = 'Drupal Web Forms';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Web Forms';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Web Forms';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'webform' => 'webform',
  );
  /* Filter criterion: Content: Status (field_webform_status) */
  $handler->display->display_options['filters']['field_webform_status_value']['id'] = 'field_webform_status_value';
  $handler->display->display_options['filters']['field_webform_status_value']['table'] = 'field_data_field_webform_status';
  $handler->display->display_options['filters']['field_webform_status_value']['field'] = 'field_webform_status_value';
  $handler->display->display_options['filters']['field_webform_status_value']['value'] = array(
    'completed' => 'completed',
  );
  $handler->display->display_options['filters']['field_webform_status_value']['expose']['operator_id'] = 'field_webform_status_value_op';
  $handler->display->display_options['filters']['field_webform_status_value']['expose']['label'] = 'Status (field_webform_status)';
  $handler->display->display_options['filters']['field_webform_status_value']['expose']['operator'] = 'field_webform_status_value_op';
  $handler->display->display_options['filters']['field_webform_status_value']['expose']['identifier'] = 'field_webform_status_value';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'webforms');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'web-forms';

  /* Display: Page - webforms status */
  $handler = $view->new_display('page', 'Page - webforms status', 'webforms_status');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'webform_edit' => 'webform_edit',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'webform_edit' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Webform: Webform edit link */
  $handler->display->display_options['fields']['webform_edit']['id'] = 'webform_edit';
  $handler->display->display_options['fields']['webform_edit']['table'] = 'node';
  $handler->display->display_options['fields']['webform_edit']['field'] = 'webform_edit';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Status (field_webform_status) */
  $handler->display->display_options['arguments']['field_webform_status_value']['id'] = 'field_webform_status_value';
  $handler->display->display_options['arguments']['field_webform_status_value']['table'] = 'field_data_field_webform_status';
  $handler->display->display_options['arguments']['field_webform_status_value']['field'] = 'field_webform_status_value';
  $handler->display->display_options['arguments']['field_webform_status_value']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['field_webform_status_value']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['field_webform_status_value']['title'] = 'Web Forms: %1';
  $handler->display->display_options['arguments']['field_webform_status_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_webform_status_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_webform_status_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_webform_status_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_webform_status_value']['limit'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'webform' => 'webform',
  );
  $handler->display->display_options['path'] = 'web-forms/%';
  $export['web_forms'] = $view;

  return $export;
}
