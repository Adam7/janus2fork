<div class="h2-block-title-wrapper">
  <h2 class="block-title"><?php print $block['subject']; ?></h2>
</div>
<p>Use the same login information here that you use for your desktop computer at work. Your user name will usually be your first initial followed by your last name (e.g., John Smith's user name would be jsmith).</p>
<p>If you're unsure of your user name, contact Systems and ask for your "LDAP" information.</p>
