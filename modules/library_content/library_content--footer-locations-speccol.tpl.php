<div class="col span_6">
  <h3>Locations</h3>
  <div class="locations">
    <div class="select-library stilts">
      <select name="liblocation" id="liblocation" tabindex="1">
        <option value="specialcol">Special Collections</option>
        <option value="lied">Lied Library</option>
        <option value="arch">Architecture Library</option>
        <option value="curr">Curriculum Materials</option>
        <option value="music">Music Library</option>
        <option value="law">Law Library</option>
      </select>
    </div>
  </div>

  <div id="lib-lied" class="lib-branch">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457001
      <br />Las Vegas, Nevada
      <br />89154-7001
      <br />
      <br /><a href="http://www.library.unlv.edu">UNLV Libraries Site &#187;</a>
      <br />(702) 895-2111
    </p>
  </div>
  <div id="lib-specialcol" class="lib-branch active">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457010
      <br />Las Vegas, Nevada
      <br />89154-7010
      <br />
      <br /><a href="/speccol">Special Collections Library Site &#187;</a>
      <br />(702) 895-2234
    </p>
  </div>
  <div id="lib-arch" class="lib-branch">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 454049
      <br />Las Vegas, Nevada
      <br />89154-4049
      <br />
      <br /><a href="/arch">Arch Library Site &#187;</a>
      <br />(702) 895-1959
    </p>
  </div>
  <div id="lib-curr" class="lib-branch">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 453009
      <br />Las Vegas, Nevada
      <br />89154-3009
      <br />
      <br /><a href="/cml">CML Library Site &#187;</a>
      <br />(702) 895-3593
    </p>
  </div>
  <div id="lib-music" class="lib-branch">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457002
      <br />Las Vegas, Nevada
      <br />89154-7002
      <br />
      <br /><a href="/music">Music Library Site &#187;</a>
      <br />(702) 895-2541
    </p>
  </div>
  <div id="lib-law" class="lib-branch">
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 451003
      <br />Las Vegas, Nevada
      <br />89154-1003
      <br />
      <br /><a href="http://law.unlv.edu/law-library/home.html">Law Library Site &#187;</a>
      <br />(702) 895-3671
    </p>
  </div>
  
  <div class="hours col alpha">
    <h5>Today&#39;s Hours:</h5>
    <?php 
      include_once ("library_content-hours.inc");
      PrintBranchHour_Today (11);
    ?>
  </div>
</div>

<div class="col span_6">
  <h3 class="placeholder">
    &nbsp;
  </h3>
  <div id="map" class="map">
  </div>
  <div class="footer-block">
    <h5>
      More Information
    </h5>
    <ul class="menu no-bullet stilts">
      <li>
        <a href="<?php print base_path(); ?>services/hours">Future Library Hours</a>
      </li>
      <li>
        <a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a>
      </li>
      <li>
        <a href="http://www.unlv.edu/parking">Parking Information</a>
      </li>
    </ul>
  </div>
</div>
<div class="col span_6">
  <h3>
    Contact
  </h3>
  <div class="footer-block">
    <h5>
      Ask Us!
    </h5>
    <ul class="menu no-bullet">
      <li>
        <a href="#" onclick="Chat=window.open('https://v2.libanswers.com/widget_standalone.php?hash=2de07cac2d72945bcdf47593b1c7308d','Chat','toolbar=no,location=no,directories=no,status, menubar=no,scrollbars,resizable,width=350,height=700'); return false;"><span class="glyph">&#xf086;</span> Chat</a>
      </li>
      <li>
        <span class="footer-text"><span class="glyph">&#xf10a;</span> Text: (702) 945-0822</span>
      </li>
      <li>
        <span class="footer-text"><span class="glyph">&#xf095;</span> Phone: (702) 895-2111</span>
      </li>
      <li>
        <a href="<?php print base_path(); ?>ask/email.php"><span class="glyph">&#xf0e0;</span> Email</a>
      </li>
    </ul>
  </div>
    <div class="footer-block">
      <h5>
        Social Media
      </h5>
      <div class="contact-group-links ">
        <ul class="menu no-bullet">
          <li>
            <a href="https://www.facebook.com/unlvspecialcollections">
              <span class="glyph facebook">&#xf082;</span> Facebook
            </a>
          </li>
          <li>
            <a href="https://twitter.com/unlvsc">
              <span class="glyph twitter">&#xf099;</span> Twitter
            </a>
          </li>
          <li>
            <a href="http://www.youtube.com/user/unlvlibraries">
              <span class="glyph youtube">&#xf04b;</span> YouTube
            </a>
          </li>
            <li>
              <a href="http://instagram.com/unlvspeccoll">
                <span class="glyph instagram">&#xf16d;</span> Instagram
              </a>
            </li>
          <li>
            <a href="<?php print base_path(); ?>contact/social_media">All Social Media</a>
          </li>
        </ul>
      </div>
    </div>
</div>
<div class="col span_6">
  <h3 class="placeholder">
    &nbsp;
  </h3>
  <div class="footer-block">
    <h5>
      Directories
    </h5>
    <ul class="menu no-bullet">
      <li>
        <a href="<?php print base_path(); ?>contact/librarians_by_subject">Librarians by Subject</a>
      </li>
      <li>
        <a href="<?php print base_path(); ?>about/staff/libstafinfo.php?style=2">Staff Directory</a>
      </li>
    </ul>
  </div>
  <div class="footer-block">
    <h5>
      Employment
    </h5>
    <ul class="menu no-bullet">
      <li>
        <a href="<?php print base_path(); ?>employment">Full-Time</a>
      </li>
      <li>
        <a href="<?php print base_path(); ?>employment/student.html">Student</a>
      </li>
    </ul>
  </div>
  <div class="footer-block">
    <h5>
      Feedback
    </h5>
    <ul class="menu no-bullet">
      <li>
        <a href="<?php print base_path(); ?>comments.php"><span class="glyph">&#xf0a1;</span> Provide Feedback</a>
      </li>
    </ul>
  </div>
</div>
<div class="col span_24">
  <p class="copyright mobilecheck">
    <a href="<?php print base_path(); ?>">© <?php echo date("Y") ?> University of Nevada, Las Vegas</a>
  </p>
  <a href="<?php print base_path(); ?>user" class="user">
    <span class="glyph">&#xf02d;</span>
  </a>
