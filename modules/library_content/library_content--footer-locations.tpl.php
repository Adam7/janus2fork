<h2 class="hack-magic alpha">Locations</h2>
<div class="locations">
  <div class="select-library stilts hack-magic alpha">
    <form method="post">
      <select id="library-select">
        <option value="lied" selected="selected">Lied Library</option>
        <option value="specialcol">Special Collections</option>
        <option value="arch">Architecture Library</option>
        <option value="curr">Curriculum Materials</option>
        <option value="music">Music Library</option>
        <option value="law">Law Library</option>
      </select>
    </form>
  </div>
  <div id="lib-lied" class="lib-branch active">
    <div class="map grid-6 col omega map-lied"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457001
      <br />Las Vegas, Nevada
      <br />89154-7001
      <br />
      <br />(702) 895-2111
    </p>
  </div>
  <div id="lib-specialcol" class="lib-branch">
    <div class="map grid-6 col omega map-lied"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457010
      <br />Las Vegas, Nevada
      <br />89154-7010
      <br />
      <br /><a href="/speccol">Library Site &#187;</a>
      <br />(702) 895-2234
    </p>
  </div>
  <div id="lib-arch" class="lib-branch">
    <div class="map grid-6 col omega map-arch"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 454049
      <br />Las Vegas, Nevada
      <br />89154-4049
      <br />
      <br /><a href="/arch">Library Site &#187;</a>
      <br />(702) 895-1959
    </p>
  </div>
  <div id="lib-curr" class="lib-branch">
    <div class="map grid-6 col omega map-curr"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 453009
      <br />Las Vegas, Nevada
      <br />89154-3009
      <br />
      <br /><a href="/cml">Library Site &#187;</a>
      <br />(702) 895-3593
    </p>
  </div>
  <div id="lib-music" class="lib-branch">
    <div class="map grid-6 col omega map-music"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 457002
      <br />Las Vegas, Nevada
      <br />89154-7002
      <br />
      <br /><a href="/music">Library Site &#187;</a>
      <br />(702) 895-2541
    </p>
  </div>
  <div id="lib-law" class="lib-branch">
    <div class="map grid-6 col omega map-law"></div>
    <p class="address grid-6 col alpha">4505 South Maryland Pkwy.
      <br />Box 451003
      <br />Las Vegas, Nevada
      <br />89154-1003
      <br />
      <br /><a href="http://law.unlv.edu/law-library/home.html">Library Site &#187;</a>
      <br />(702) 895-3671
    </p>
  </div>
  <div class="footer-more-hours grid-6 col omega">
    <div class="footer-block">
      <h5>
          More Information
      </h5>
      <ul class="no-bullet">
        <li>
          <a href="/services/hours">Future Library Hours</a>
        </li>
        <li>
          <a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a>
        </li>
        <li>
          <a href="http://www.unlv.edu/parking">Parking Information</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="hours grid-6 col alpha">
    <h5>Today&#39;s Hours:</h5>
      <?php include_once ("library_content-hours.inc");
            PrintBranchHour_Today (2);
      ?>
  </div>
</div>
