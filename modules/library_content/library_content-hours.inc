<?php

//============================================================================
function PrintBranchHour_Today ($active_id=2)
// $active_id: category id, integer. Used to set active html tag.
{
    // Connect to database.

        // Database server names.
    $_DB_SERVER_WEBDB1_ = 'vmdb.library.unlv.edu';
    $_DB_NAME_CAL_ = 'vcalendar';

        // Shared login and password across databases listed below.
    $_DB_LOGIN_WEBDB_ = 'calreader';
    $_DB_PASSWD_WEBDB_ = 'jump.start';

    $_dblnk_cal_ = mysql_connect($_DB_SERVER_WEBDB1_, $_DB_LOGIN_WEBDB_, $_DB_PASSWD_WEBDB_);

    if ( FALSE === $_dblnk_cal_ )
    {
        echo "We encountered difficulties with our calendar. Sorry for the inconvenience. We will fix the problem soon.";
        return;
    }

    mysql_select_db($_DB_NAME_CAL_, $_dblnk_cal_);

        // Set connection character set.
    mysql_query ("SET CHARACTER SET 'UTF-8'", $_dblnk_cal_);
    mysql_query ("SET NAMES 'utf8'", $_dblnk_cal_);




    // Get today's date, yyyy-mm-dd
$today = date ('Y-m-d');


    // Define category ids. These are hard-coded from database data,
    // since there's no way to match category name with webpage's paragraph
    // title. So just do it here.
    // Add new code if new category is added.
    // The array keys is ordered by which category shows first.
    // DO NOT CHANGE ARRAY ELEMENT ORDER UNLESS HAVE TO.
    // Array keys are the category_id from database definition.
$cal_cats = array (
    2   => array (
        'TITLE' => 'Lied (Main Library)',
//        'LINK_CAL'  => '/calendar/index.php?categories=2',
//        'LINK_WEB'  => '/',
        'LINK_CAL'  => '',
        'LINK_WEB'  => '',
        'PREFIX'    => 'Lied:',
        'id_css'    => 'lib-lied-hours',
    ),

    11   => array (
        'TITLE' => 'Special Collections',
        'LINK_CAL'  => '/calendar/index.php?categories=11',
        'LINK_WEB'  => '/speccol/',
        'PREFIX'    => 'Special Collections:',
        'id_css'    => 'lib-specialcol-hours',
    ),

    7   => array (
        'TITLE' => 'Architecture Studies Lib.',
        'LINK_CAL'  => '/calendar/index.php?categories=7',
        'LINK_WEB'  => '/arch/',
        'PREFIX'    => 'ASL:',
        'id_css'    => 'lib-arch-hours',
    ),

    10   => array (
        'TITLE' => 'Curriculum Materials Lib.',
        'LINK_CAL'  => '/calendar/index.php?categories=10',
        'LINK_WEB'  => '/cml/',
        'PREFIX'    => 'CML:',
        'id_css'    => 'lib-curr-hours',
    ),

    9   => array (
        'TITLE' => 'Music Library',
        'LINK_CAL'  => '/calendar/index.php?categories=9',
        'LINK_WEB'  => '/music/',
        'PREFIX'    => 'Music:',
        'id_css'    => 'lib-music-hours',
    )
);


    // Set up which categories to print, by looking at $cal_category.
    // Set default to be all branches.
if ( ! isset ($cal_category) || ! is_array ($cal_category) )
    $cal_category = array_keys ($cal_cats);


foreach ( $cal_category as $cal_id )
{
    if ( array_key_exists ($cal_id, $cal_cats) )
    {
        $id_css = $cal_cats[$cal_id]['id_css'];


$id = $cal_id;
$info = $cal_cats[$cal_id];
$dblnk = $_dblnk_cal_;



    $query = "SELECT event_title FROM events WHERE category_id='$id' AND"
        . " event_date='$today' AND event_is_public='1'";

    $result = mysql_query ($query, $dblnk);
    if ( FALSE !== $result && mysql_num_rows ($result) > 0 )
        // Only take the first row.
    {
        $line = mysql_fetch_assoc ($result);
        $hour = $line['event_title'];

            // Strip off prefix from hour.
        $hour = str_replace ($info['PREFIX'], '', $hour);
        $hour = trim ($hour);

        if ( $active_id == $id )
            echo "<div id=\"$id_css\" class=\"lib-branch active\">\n";
        else
            echo "<div id=\"$id_css\" class=\"lib-branch\">\n";

            // Print title.
        echo "<p>$hour</p>\n";
        echo "</div>\n";
    }


    }
}

mysql_close ($_dblnk_cal_);

return;

}


?>
