<?php
/**
 * @file
 * libsearch.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function libsearch_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'selectmenu_form_id_exceptions';
  $strongarm->value = 'webpac-title
webpac-author
webpac-subject
subjmenu
page-node-form';
  $export['selectmenu_form_id_exceptions'] = $strongarm;

  return $export;
}
