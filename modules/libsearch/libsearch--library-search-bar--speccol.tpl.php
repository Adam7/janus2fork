<div class="search-tool-wrapper clearfix">
  <p class="more-research">
    <strong>Links: </strong><a href="http://www.library.unlv.edu">University Libraries</a>, <a href="http://www.library.unlv.edu/speccol/visit/contact">Contact Special Collections</a>
  </p>
  <form id="ui_element" class="sb_wrapper clearfix" action="https://unlv.summon.serialssolutions.com/search">
    <p>
      <span id="sb_arrow" class="sb_down"></span>
      <input class="sb_input" type="text" name="s.q" autocomplete="off">
      <input class="sb_search" type="submit" value="">
    </p>
    <ul class="sb_dropdown" style="display: none;">
			<div id="sc-search">
		    <ul>
		      <li>
		        <input id="r1" type="radio" name="search-type" value="search-summon" checked>
		        <label for="r1" title="Search the Libraries' books, articles, and more">
		        	<span class="fa-radio"></span><span class="glyph">&#xf0e7;</span><span class="label"> Quick Search</span>
			        <div class="sub-description">Books, full-text articles, and more</div>
		        </label>
		      </li>
		      <li>
		        <input id="r2" type="radio" name="search-type" value="search-catalog" data-value="http://webpac.library.unlv.edu/search~S1/X">
		        <label for="r2" title="Search for Special Collections' books, periodicals, maps, and manuscripts">
		        	<span class="fa-radio"></span><span class="glyph">&#xf02d;</span><span class="label"> Catalog</span>
		        	<div class="sub-description">Books, periodicals, and more in Special Collections</div>
		        </label>
		      </li>
		      <li>
		        <input id="r3" type="radio" name="search-type" value="search-speccoldatabase" data-value="http://www.library.unlv.edu/speccol/databases/index.php">
	          <label for="r3" title="Search for photographs, oral histories, manuscripts, and University Archives">
	          	<span class="fa-radio"></span><span class="glyph">&#xf0ce;</span><span class="label"> Special Collections Database</span>
	          	<div class="sub-description">Oral histories, photographs, manuscripts, and University Archives</div>
	          </label>		        
		      </li>
		      <li>
		        <input id="r4" type="radio" name="search-type" value="search-manuscript" data-value="http://www.library.unlv.edu/speccol/finding-aids-search.html">
		        <label for="r4" title="Search detailed descriptions of select manuscript collections">
		        	<span class="fa-radio"></span><span class="glyph">&#xf002;</span><span class="label"> Manuscript Finding Aids</span>
		        	<div class="sub-description">Guides to select manuscript collections</div>
		        </label>
		      </li>
		      <li>
		        <input id="r5" type="radio" name="search-type" value="search-digital" data-value="http://d.library.unlv.edu/cdm/search/">
		        <label for="r5" title="Search for digital photographs and other digitized materials">
		        	<span class="fa-radio"></span><span class="glyph">&#xf108;</span><span class="label"> Digital Collections</span>
		        	<div class="sub-description">Digitized items available on online</div>
		        </label>
		      </li>
		      <li>
		      	<input id="r6" type="radio" name="search-type" value="search-library-info" data-value="http://www.library.unlv.edu/search/googleresults.html">
		      	<label for="r6" title="Search the content of the Library's website">
		      		<span class="fa-radio"></span><span class="glyph">&#xf0a6;</span><span class="label"> Library Information</span>
		        	<div class="sub-description">Library Information</div>
		      	</label>
		      </li>
		    </ul>
			</div>
		</ul>
	</form>
  <div class="search-help">
  	<a href="http://www.library.unlv.edu/speccol/research_and_services/finding_our_materials"><span class="glyph"></span></a>
	</div>
</div>
