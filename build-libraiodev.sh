#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ..
DIRUP1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd html
DSTAMP=$(date +"%Y%m%d%H%M") 
# drush make for building libraiodev environment
drush make make-build/janus2-libraiodev.build /var/www/html/builds/$DSTAMP

# creating file structure drupal expects
echo "creating file structure drupal expects..."
# create and link files dir
echo "creating files directory..."
if [ ! -d /var/www/html/files ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  mkdir /var/www/html/files
  mkdir /var/www/html/files/public
  mkdir /var/www/html/files/private
  chown vagrant:apache -R /var/www/html
  chmod 775 -R /var/www/html/files
  # TODO: nest else when files directory exists
fi

# linking files directories
ln -s /var/www/html/files/public /var/www/html/builds/$DSTAMP/sites/default/files
ln -s /var/www/html/files/private /var/www/html/builds/$DSTAMP/sites/default/private

# prepare settings file
# if settings.php exists link to it, else copy it and link to it
if [ -f /var/www/html/config/settings.php ]
  then
    chown vagrant:apache /var/www/html/config/settings.php
    chmod 775 /var/www/html/config/settings.php
    ln -s /var/www/html/config/settings.php /var/www/html/builds/$DSTAMP/sites/default/settings.php
  else
    mkdir /var/www/html/config
    cp /var/www/html/builds/$DSTAMP/sites/default/default.settings.php /var/www/html/config/settings.php
    chown vagrant:apache /var/www/html/config/settings.php
    chmod 775 /var/www/html/config/settings.php
    ln -s /var/www/html/config/settings.php /var/www/html/builds/$DSTAMP/sites/default/settings.php
fi
# echo "securing the settings file and parent directory."
# chmod g-w /var/www/html/config/settings.php
# chmod g-w /var/www/html/builds/$DSTAMP/sites/default

### Dev Only ###
# link custom modules
#echo "linking custom modules..."
#ln -s $DIR/modules ../html/builds/$DSTAMP/sites/all/modules/custom

# link custom themes
echo "linking custom themes..."
ln -s /var/www/html/themes /var/www/html/builds/$DSTAMP/sites/all/themes/custom

# link libraries
if [ -e /var/www/html/builds/$DSTAMP/profiles/libraries ]; then
  echo "linking libraries...";
  ln -s /var/www/html/builds/$DSTAMP/profiles/libraries /var/www/html/builds/$DSTAMP/sites/all/libraries
#  echo " fixing flexslider lib...";
#  mv $DIRUP1/html/builds/$DSTAMP/sites/all/libraries/flexslider/FlexSlider-2.1/* $DIRUP1/html/builds/$DSTAMP/sites/all/libraries/flexslider/
fi
# link to latest build
if [ -L /var/www/html/drupal ]; then
  rm /var/www/html/drupal
fi
ln -s builds/$DSTAMP /var/www/html/drupal

echo "done. enjoy yourself."