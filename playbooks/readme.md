# How to use playbook with Vagrant
Run the following command from project root:  

`$ ansible-playbook playbooks/yum-libraiodev-vagrant.yml -u vagrant -s -k`  

This will ask for the root password, which is `vagrant`