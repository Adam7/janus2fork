UNLV University Libraries Website
=================================

This repo contains the build scripts and the source code for custom development on the UNLV University Libraries Website (http://library.unlv.edu).

Using Ansible 1.4.3 you can build a dev environment to a local virtual machine supplied by Virtualbox and managed by vagrant.

## Ansible usage
Use this command when using with vagrant:
`ansible-playbook playbooks/yum-libraiodev.yml -k -s`